FROM alpine:latest

RUN apk add --no-cache clang lld yasm make ninja xorriso mtools grub-bios grub-efi
RUN mkdir /root/osdev

WORKDIR /root/osdev
#CMD time ninja
CMD sh -c 'time make all'
